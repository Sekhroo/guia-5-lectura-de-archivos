#! usr/bin/env python3
import os
database = 'co2_emission.csv'

(PAIS,CODIGO,
 AGE,CO2) = range(4)

def opendata():
    temp = open(database)
    arch_dic = {}

    for counter, linea in enumerate(temp):
        if counter != 0:
            temp_linea = linea.split(',')
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            age = temp_linea[AGE].strip()
            co2 = temp_linea[CO2].strip()
            
            temp_dic = {'codigo': codigo,
                        age: co2}
            
            if arch_dic.get(pais):
                arch_dic[pais].update(temp_dic)
            else:
                arch_dic[pais] = temp_dic
    temp.close()

    return arch_dic

def conta_paise():
    temp = open(database)
    arch_dic = {}
    dic_cont = {}
    a = int(0)
    b = str

    for counter, linea in enumerate(temp):
        if counter != 0:
            temp_linea = linea.split(',')
            pais = temp_linea[PAIS].strip() 
            if pais != b:
                a = 0
            if arch_dic.get(pais):
                dic_cont[pais].update()
            else:
                a = a + 1
                b = pais
                dic_cont[pais] = a
    temp.close()
    return dic_cont

def promedio_paises(data, dic_cont):
    cont = int(0)
    sumaMAX = int(0)
    for key in dic_cont:
        suma = int(0)
        arch_dic = data[key]
        cont = 1 + cont
        for key, value in arch_dic.items():
            if key != 'codigo':
                suma = suma + float(value)
        sumaMAX = suma + sumaMAX
    promediopaises = sumaMAX / cont
    return promediopaises

def porj(promediopaises,data,dic_cont):
    diezpor = promediopaises*0.1
    diezpor = diezpor/2
    promediomas = promediopaises + diezpor
    promediomenos = promediopaises - diezpor
    regis_pais = ['Pais']
    a_value = int(0)
    b_value = int(0)
    contador = int(0)
    for key in dic_cont:
        cont = int(0)
        suma = int(0)
        sumaMAX = int(0)
        arch_dic = data[key]
        paises = key
        for key, value in arch_dic.items():
            if key != 'codigo':
                cont = cont + 1
                suma = suma + float(value)
            sumaMAX = suma + sumaMAX
        promedio = sumaMAX / cont
        
        a_value = promedio
        if a_value > promediomenos and a_value < promediomas:
            b_value = a_value
            paisMenor = paises
            regis_pais.append(paisMenor)
            contador = contador + 1
    print (f'Los paises que se encuentran en el margen de error son {regis_pais}')

        
def main():
    arch_dic = opendata()
    dic_cont = conta_paise()
    promediopaises = promedio_paises(data=arch_dic, dic_cont=dic_cont)
    porj(promediopaises,data=arch_dic, dic_cont=dic_cont)
    
main()