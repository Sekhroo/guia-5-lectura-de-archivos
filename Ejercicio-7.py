#! usr/bin/env python3
import os
database = 'co2_emission.csv'

(PAIS,CODIGO,
 AGE,CO2) = range(4)

def opendata():
    temp = open(database)
    arch_dic = {}

    for counter, linea in enumerate(temp):
        if counter != 0:
            temp_linea = linea.split(',')
            #print(temp_linea[co2].strip())
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            age = temp_linea[AGE].strip()
            co2 = temp_linea[CO2].strip()
            
            temp_dic = {'codigo': codigo,
                        age: co2}
            
            if arch_dic.get(pais):
                arch_dic[pais].update(temp_dic)
            else:
                arch_dic[pais] = temp_dic
    temp.close()

    return arch_dic
def buscar_pais():
    pais = str(input('¿Qué pais desea buscar?\n'))
    pais = pais.lower()
    pais = pais.title()

    return pais
def muestra_data(data,pais):
    arch_dic = data[pais]
    suma = 0
    for key, value in arch_dic.items():
        if key != 'codigo':
            suma = suma + float(value)
    print(suma)
def main():
    dic = opendata()
    pais = buscar_pais()
    muestra_data(data=dic,pais=pais)

resp = str
while resp != 'n' and resp != 'N':
    os.system ("clear")
    main()
    resp = str
    while resp != 's' and resp != 'S' and resp != 'n' and resp != 'N':
        resp = str(input('¿Desea buscar otro pais? (s/n)\n'))
        if resp != 's' and resp != 'S' and resp != 'n' and resp != 'N':
            print('El valor no es valido\n')
